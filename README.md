# Website voor Sjoerd Hoogerhuis

## Installatie
1. [Download Zola](https://www.getzola.org/).
2. Clone of download deze repository.

## Gebruik
3. Verwijder de test posts.
4. Maak posts aan in de `content/blog` map in Markdown syntax. Zie [hier](https://www.markdownguide.org/basic-syntax) voor een uitleg van Markdown.
5. Om te testen, voer `zola serve` uit in deze map.
6. Om online te zetten, voer `zola build` uit in deze map. De gegenereerde bestanden komen dan in de `public` map te staan. Deze kan je dan kopieren naar de live versie van je website.

# Extra info
- [Colorscheme](https://coolors.co/fff7f0-7b9e87-5e747f-da7422-ff84e8)
- [Zola Website](https://www.getzola.org/)
- [FontAwesome](https://fontawesome.com/) wordt gebruikt voor de icons.
- [Chart.js](https://www.chartjs.org/) wordt gebruikt om grafieken te maken.
- Zola gebruikt [Sass](https://sass-lang.com/) voor stylesheets.

# Shortcodes
Zola heeft een functie genaamd shortcodes waarmee je makkelijk stukken HTML in Markdown kan gebruiken. Een voorbeeld hiervan is de `youtube` functie. Deze wordt gebruikt op de volgende manier:
```
{{ youtube(id=..., class=..., autoplay=...) }}
```
Er is een custom shortcode voor images met een caption. Het gebruik daarvan is als volgt:
```
{{ image(path=..., caption=...) }}
```
Zie de videokaart-showdown post voor voorbeelden van image en Test 6 voor youtube.