+++
title = "De Videokaart Benchmark Shit Showdown"
date = 2020-04-01
description = "De strijd tussen 5 sterk verouderde graphische kaarten"

[extra]
cover = "image1.jpeg"
+++

In het volgende artikel nemen we vijf sterk verouderde en ook in de tijd niet high-end videokaarten onder de loep. De vier benchmarks, allevier even irrelevant in 2020, zijn als volgt:

1. **Unigine Valley – 1080p low**. De keuze voor Unigine Valley in plaats van bijvoorbeeld Heaven of de nieuwere Supersposition is gemaakt om twee redenen. Ten eerste vind ik Valley de mooiste benchmark van de drie (alhoewel Superposition ook prachtig is), en ten tweede is Valleyde makkelijkste benchmark voor dit slag videokaarten. Zoals later in de resultaten zal blijken was zelfs deze relatief makkelijke benchmark, nota bene afgesteld op laag, te veel voor de kaarten.
2. **Just Cause 2 – Desert Sunrise**. Minstens even irrelevant als Valley is Just Cause 2 in 2020. Het spel staat eigenlijk bekend om een ding, en dat is de enorme open wereld. Spectaculair voor 2010-maatstaven, en tegenwoordig met meer dan 400 vierkante kilometer nog steeds groot. De gameplay en het verhaal worden vaak genoemd als minder bewonderenswaardig. Dat deze bevinding niet voor mij geldt is aan deze keuze wel af te lezen. Just Cause 2 is het spel dat ik mij later zal herinneren als ik aan mijn tienerjaren denk. De bewegingsvrijheid, het brede scala aan voertuigen en het tropische eiland Panau creëren een atmosfeer waar tot nu toe geen enkel ander spel dat ik gespeeld heb aan heeft kunnen tippen. De roze nostalgiebril waar ik dit spel door bekijk zou ik het liefst omwisselen voor een VR-headset voor als er ooit een remaster of mod komt die dit toe laat. We dwalen af. Deze benchmark heb ik gekozen omdat het de lichtste benchmark van de drie in Just Cause 2 is. De relatief open omgeving en toch wel mooie setting geven een goede indruk van een best-case scenario van doorsnee Just Cause 2 gameplay.
3. **Just Cause 2 – Concrete Jungle**. Een minder fraaie benchmark uit Just Cause 2 is Concrete Jungle. Deze speelt zich af in het dichtbebouwde Panau City, en is daarmee de zwaarste benchmark in Just Cause 2. Door de grote hoeveelheid objecten en lichteffecten in de benchmark scoren alle kaarten in deze test slechter op Concrete Jungle dan op Desert Sunrise
4. **Far Cry 2**. De oudste benchmark in deze test is Far Cry 2. Ondanks de lange tijd sinds de uitgave van dit spel is het nog altijd vrij zwaar, zelfs voor moderne hardware. Niet op een haal-de-brandblusser-hij-heeft-Crysis-op-medium-gezet niveau, maar een soort Crysis, made in Africa.Want Far Cry 2 speelt in Afrika. De uitgewassen kleuren, niet helemaal achterlijke AI en onvergeeflijke gameplay elementen zorgen voor een hele aparte setting voor een Far Cry spel. Voor wie Far Cry 3 te makkelijk of niet grimmig genoeg vond, probeer het tweede deel van de serie eens. De multiplayer is trouwens ook zeer geschikt voor LAN parties. In tegenstelling tot Just Cause 2 heeft Far Cry 2 een opzichzelfstaand programma om mee te benchmarken. Hierin zijn alle instellingen aan te passen en is het mogelijk om een reeks van meerdere benchmarks met meerdere configuraties achter elkaar te laten lopen.

Tijd om de sterren van de show voor te stellen. Deze test bestond oorspronkelijk uit drie generaties van dezelfde videokaart. De GT 520, GT 620 en GT 720. Omdat ik verwachtte dat de twee andere videokaarten die ik had liggen, de GT 140 en HD 5670 ongeveer van hetzelfde verschrikkelijke niveau zouden zijn heb ik deze ook getest, om de grafieken wat interessanter te maken. Zoals men ook wel eens zegt dat de weg naar de herberg interessanter is dan de herberg zelf zijn ook de verhalen hoe ik aan deze kaarten gekomen ben interessanter dan de kaarten zelf.

{{ image(path="image1.jpeg", caption="v.l.n.r.: De GT 140, GT 520, GT 620, GT 720 en HD 5670.")}}

Te beginnen met de slechtste kaart van de test, de GT 520. Deze kaart is bij mij terecht gekomen door middel van een sinterklaas-surprise van een vriend. Hij had als surprise een oude Acer SFF desktop gekregen, maar dan met dertig extra gaten en schroeven erin die er een voor een uit gehaald moesten worden om binnen te komen. Er zaten alleen ook nog een paar onderdelen in de kast. Een socket 1155 moederbord met een standaard 24-pins aansluiting (met een i5-2300), een wifi-kaart die later de reden bleek te zijn dat de computer in de eerste plaats was weggedaan en deze videokaart, een low profile GT 520. Deze heeft eerst nog even in een Pentium 4 klasse Optiplex gezeten om de framerate in Minecraft wat op te krikken, totdat hij vervangen werd door de GT 720 waar we zo bij komen. Met 1 GB DDR3 en kloksnelheden die voor het jaar van uitbrengen al twijfelachtig waren is deze kaart niet een van de favorieten van vandaag.

De eerste tegenstander van de GT 520 is de GT 620. Deze kaart heb ik gekregen als deel van een partijtje onderdelen dat ik van Marktplaats gehaald heb. In mijn (en dat noem ik nu zo maar eigenlijk baal ik er nog steeds van) jeugdelijke onwetendheid zag ik een DDR sleuf aan voor een DDR3 sleuf en werd zo de trotse eigenaar van een 775 bord in een onbruikbare afmeting. Ten minste zat er wel een mooie koeler bij (maar helaas ook voor socket 775) en was dit kaartje de kers op de taart van mijn miskoop. Hij deed het gelukkig wel!De GT 620 heeft net als de GT 520 1 GB DDR3 aan boord. Daarnaast zijn de kloksnelheden van de kern en het geheugen zo goed als hetzelfde, iets dat in de resultaten naar voren zal komen. Het enige verschil tussen deze twee uitvoeringen is dat deze kaart een wat robuuster uitziende VGA-aansluiting heeft. Esthetisch gezien in ieder geval een verbetering ten opzichte van het platte kabeltje dat bij de GT 520 hetzelfde doel heeft.

De eerste wat modernere kaart van vandaag (en ook de eerste die niet uit een OEM PC komt) is de GT 720. Deze kaart, tevens een low profile variant, is een vroegere upgrade van de SFF PC van het broertje van dezelfde vriend waar de GT 520 vandaan komt. Deze deed zijn oude Core 2 Quad HP computer weg toen hij een nieuwe kocht met Ryzen en RGB (geef hem eens ongelijk). Deze MSI variant is vandaag ook de enige met passieve koeling. Alhoewel deze voldoet en de kaart koel genoeg houdt heb ik er tijdens het benchmarken een 120mm X2 products ventilator op gezet om te voorkomen dat er gas terug wordt genomen bij een te hoge temperatuur. Niet alleen heeft de GT 720 een extra gigabyte VRAM aan boord, maar dankzij nieuwe architectuur en hogere kloksnelheden is het te verwachten dat deze kaart het beter gaat doen dan de eerste twee.

De vierde en laatste Nvidia kaart is de GT 140. Behalve dat dit de oudste kaart is die meedoet is het ook de kaart die de meeste stroom vereist. Dankzij de 6-pin PCIe stekker die deze kaart nodig heeft om optimaal te werken was ik vroeger aangewezen op dubbele molex naar 6-pin verloopstukjes op 300w voedingen van matige kwaliteit. Zo niet vandaag. Het verloopstukje is vervangen door een voeding die van nature een 6-pin stekker heeft, alleen is de kwaliteit van de voeding nog steeds niet om over naar huis te schrijven. Dit blok chinesium dat beweert 500w aan stroom te kunnen voorzien is lichter dan de videokaart waarvoor hij uit de kast getrokken is en is voor redenen die mij de pet boven gaan niet overgegaan tot zelfvernietiging tijdens de benchmarks. Terug naar de kaart. Deze komt ook vandaan bij de familie van deze zelfde vriend (thanks Roy! :)), dit keer uit de computer van een oom. De GT 140 is misschien niet een kaart die gelijk tot de verbeelding spreekt. Dit komt doordat de GTX 100-serie, net als de GTX 800-serie niet echt bestaan heeft. De 100-serie was een heruitgave van de welbekende 8000-serie, maar dan voor OEMs, en de 800-serie heeft alleen bestaan in laptopland. De 512mb DDR3 zullen de kaart niet echt helpen vandaag, maar de kloksnelheden en het feit dat het een 40 in plaats van een 20 kaart is zal in zijn voordeel spelen. Hoe deze kaart in het bijzonder het gaat doen ben ik ook benieuwd naar omdat het toenmalige vriendje van mijn zusje hier een half jaar op gegamed heeft voor hij upgradede naar een RX 460.

Als laatste gaan we vandaag kijken naar de prestaties van de Radeon HD 5670. In de naam klinkt het als heel wat, maar aangezien je een doctoraat in de electrofysica nodig hebt om hun benamingsschema te begrijpen is dit niet echt een high-end videokaart. Deze kaart heb ik uit een computer gevist die een nacht of twee buiten in de regen had gestaan. Samen met een MSI platinum moederbord met een Phenom 9850en een Cooler Master 650w voeding die zo vies was dat ik niet eens geprobeerd heb hem schoon te maken zat dit kaartje in deze computer vies te zijn. Tijdens de wat oppervlakkige schoonmaakbeurt die de kaart toen gekregen heeft heb ik ook geen koelpasta op de koeler gedaan om te kijken of dit een probleem was. Dat was het niet. Met of zonder koelpasta presteert deze AMD kaart hetzelfde, wat mijn leven weer wat makkelijker maakt. Zoals zo directin de grafieken zal blijken heeft dit natte verleden de kaart niets afgedaan in prestaties.

{{ image(path="image2-4.jpeg", caption="v.l.n.r.: Het ASUS P6T WS Pro moederbord, de Western Digital harde schijf en het RAM geheugen")}}

En dan het systeem waarop getest is. Ik heb hier gekozen voor een i7-920 op een ASUS P6T WS Professional moederbord om iedere mogelijke processorbottleneck te voorkomen. Niet dat dat een probleem zou zijn met dit slag videokaarten, maar het gaat om het idee. Daarnaast vind ik het ook
gewoon een erg mooi moederbord en leuk om eens te werken met een x58 systeem. Door de triple channel opbouw van dit platform heb ik gekozen voor 6 GBDDR3-1333 (die door het moederbord op 1066 geklokt werden). Als opslag stond het besturingssysteem (het verschrikkelijk voor oudere hardware geoptimaliseerde Windows 10 x64) en de benchmarks op een Western Digital WD1001FALS harde schijf uit hun black serie. De claims dat deze beter presteert dan de blue en green schijven van hetzelfde merk heb ik niet kunnen testen, maar aan de hoeveelheid geluid die kenmerkend is voor deze schijven is in ieder geval te horen dat hij zijn best doet. Ook heeft deze schijf een hele bad sector, wat niet echt een probleem is aangezien de schijf maar voor een klein deel in gebruik was. Op geen manier relevant aan de tests maar wel noemenswaardig is dat de twee ingebouwde gigabit ethernet poorten niet zichtbaar zijn voor het besturingssysteem (ongeacht of het Windows 10, 8.1 of het officieel door ASUS ondersteunde 7 was). Later kwam ik erachter dat dat kwam doordat een van de Realtek chips een fysieke beschadiging had, altijd een risico met tweedehands hardware. Om dit probleem op te lossen heb ik een Realtek 8189C PCI “fast” ethernet kaart in de onderste PCI sleuf gedaan. Deze werkte zonder problemen of aparte driver installatie met Windows 10.

Nu door naar het deel van dit artikel waar jullie na vierpagina’s gelul over nauwelijks functionele hardwareen jeugdsentiment wel aan toe zijn: de resultaten.

# Unigine Valley
<canvas id="unigine-chart" width="400" height="300"></canvas>
<script>
var colors = ['#5E747F', '#7B9E87', '#DA7422']
var chartOptions = {
    scales: {
        yAxes: [{
            ticks: {
                min: 0
            }
        }]
    }
}
window.addEventListener('load', () => {
    var ctx = document.getElementById('unigine-chart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['GT 520', 'GT 620', 'GT 720', 'GT 140', 'HD 5670'],
            datasets: [
                {
                    label: 'Low',
                    backgroundColor: colors[0],
                    data: [5.1, 5.4, 5.8, 8.9, 8.4]
                },
                {
                    label: 'Average',
                    backgroundColor: colors[1],
                    data: [8.2, 8.6, 10.1, 16.4, 17.3]
                },
                {
                    label: 'High',
                    backgroundColor: colors[2],
                    data: [14, 14.7, 18.3, 26.6, 32.8]
                }
            ]
        },
        options: chartOptions
    })
})
</script>
In Unigine Valley is te zien dat de prestaties van de drie 20ers heel weinig van elkaar verschilt. Met name het verschil tussen de GT 520 en GT 620 is hier verwaarloosbaar. De GT 720 komt iets beter uit de bus, wat waarschijnlijk te danken is aan de 2gb VRAM. Van onze twee kaarten in de iets-beter categorie presteert de HD5670 het bestequa gemiddelde framerate. De GT 140 zit hem op de hielen met 0,9 FPS minder, een minimaal verschil. Waar de HD5670 duidelijk de betere kaart is is de maximale framerate. Helaas voor deze eenzame verdediger van het AMD kamp wordt de maximale framerate doorgaans niet gezien als een hele belangrijke maatstaaf. Het is niet voor niets dat verreweg de meeste serieuze benchmarkers kijken naar de 1% en 0.1% laagste framerates. Van deze drie metingen is dan ook het dieptepunt in de benchmark interessanter dan de top. Hier laat de GT 140 met wederom een uiterst klein verschil zien dat 512mb VRAM in ieder geval in Unigine Valley geen heel groot probleem is.

# Just Cause 2 - Desert Sunrise & Concrete Jungle
<canvas id="justcause-chart" width="400" height="300"></canvas>
<script>
window.addEventListener('load', () => {
    var ctx = document.getElementById('justcause-chart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['GT 520', 'GT 620', 'GT 720', 'GT 140', 'HD 5670'],
            datasets: [
                {
                    label: 'Concrete Jungle',
                    backgroundColor: colors[1],
                    data: [9.98, 10.53, 13.11, 21.99, 16.11]
                },
                {
                    label: 'Desert Sunrise',
                    backgroundColor: colors[0],
                    data: [19.1, 20.14, 24.24, 39.59, 33.11]
                }
            ]
        },
        options: chartOptions
    })
})
</script>
Het inbouwen van een benchmark in de meeste spellen betekent tegenwoordig dat er aan het einde van de test een mooi lijstje met resultaten uitrolt, klaar om vergeleken te worden met andere hardwareconfiguraties. Helaas was het toevoegen van twee toch wel simpel bij te programmeren variabelen in de Just Cause 2 benchmark te veel gevraagd, vandaar dat de benchmark na voltooiing alleen een gemiddelde geeft. De hogere scores zijn van de minder intensieve Desert Sunrise benchmark en de lagere scores van de zwaardere Concrete Jungle.Ook in Panau is te zien dat de GT 520 en GT 620 erg op elkaar lijken qua prestaties. Minimale winst wordt door de GT 620 geboekt in beide benchmarks, waarna de GT 720 beide kaarten drie àvijf frames verder het nakijken geeft. Dat ook in Just Cause 2 (vrij hoog ingesteld) 512mb geen probleem vindt is te zien aan de prestaties van de GT 140. Waar de GT 720 in Desert Sunrise al aardig in de buurt van de vaak als speelbaar geziene 30 fps komt is dit in Concrete Jungle anders. 13 fps is dan weer wel lekker authentiek. Ik denk dat de HD 8570D, die in mijn AMD A8-6600k ingebakken zit nog steeds niet te spreken is over de tijd dat ik mijn dagelijkse twee uurtjes explosies of survival in de multiplayer genoot met 14 hele frames per seconde. De HD5670, die hier opklimt naar een niveau wat je van een GT 820 zou verwachten heeft moeite met deze benchmarks. Ik vermoed dat dit iets te maken heeft met drivers of optimalisatie aan de kant van Just Cause. Het Nvidia logo dat zich bij iedere start van het spel aandoet ondersteunt dit.

# Far Cry 2
<canvas id="farcry-chart" width="400" height="300"></canvas>
<script>
window.addEventListener('load', () => {
    var ctx = document.getElementById('farcry-chart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['GT 520', 'GT 620', 'GT 720', 'GT 140', 'HD 5670'],
            datasets: [
                {
                    label: 'Low',
                    backgroundColor: colors[0],
                    data: [28.76, 29.84, 58.65, 34.53, 56.19]
                },
                {
                    label: 'Average',
                    backgroundColor: colors[1],
                    data: [42.02, 43.6, 78.03, 51.75, 92.01]
                },
                {
                    label: 'High',
                    backgroundColor: colors[2],
                    data: [65.61, 68.33, 109.82, 81.21, 162.75]
                }
            ]
        },
        options: chartOptions
    })
})
</script>
Far Cry 2 laat een ander beeld zien. Weer zijn de GT 520 en 620 erg hetzelfde, maar daar houdt het zo’n beetje op. De GT 720 laat hier echt zien wat strijden zonder ventilator betekent en verdubbelt de framerates van de GT 520 en 620 bijna hier. De grote teleurstelling van deze benchmark is de GT 140. Alhoewel de framerate zeker speelbaar is is dit niet de verbetering ten opzichte van de GT 720 die we op basis van de vorige benchmarks verwachtten. Zelfs voor moderne hardware kan Far Cry 2 soms nog uitdagendzijn, dus misschien is dit waar het doek valt voor de 512mb VRAM op deze kaart. Verassend hier is de HD 5670. Waar het laagste punt iets lager is dan de GT 720 is het gemiddelde en het hoogste punt veel beter dan de concurrentie. Zelfs de laagst gemeten framerate hier is met 56 heel dicht in de buurt van die felbegeerde 60 fps. Een gemiddelde van 92 is meer dan je doorsnee monitor weer kan geven, en met een astronomische 162 fps moet je van goeden huize komen om daar optimaal van te kunnen genieten.

{{ image(path="image5.png", caption="v.bo.n.bn.: De GT 520, GT 620, GT 720, HD 5670 en GT 140") }}

# Ten slotte
In conclusie, had het zin? Nee. Was het leuk? Ja. Is het voor herhaling vatbaar? Ja, maar met een kanttekening. Het probleem hier is dat het moederbord, samen met de koeler zijn verkocht aan een Belg uit het schilderachtige Lokeren. De processor heb ik nog, dus met een ander x58 moederbord zijn de tests te herhalen en te vergelijken met de tests van vandaag. Aan de andere kant kan ik me ook niet voorstellen dat er factoren in deze computer waren die voorkomen hebben dat de videokaarten optimaal presteerden. Menig dubieus 1155 ik-kom-uit-een-rebuilt-waar-ze-maximaal-bezuinigd-hebben-op-alles-behalve-de-processor bordje kan met een doorsnee i5 in een voldoendeniveau voorzien qua processorprestaties. Andere benchmarks zijn voor een volgende keer ook interessant. Wat modernere games of wellicht een render in video export of Blender? Ook kijken naar andere statistieken als stroomverbruik, kloksnelheid, geheugengebruik en temperatuur zijn interessant om een completer beeld te geven van de kaarten in de praktijk.De overall winnaar van vandaag is de HD5670 wat mij betreft. De beste in Unigine Valley, en met afstand de beste kaart in Far Cry 2. Minder goed in Just Cause, maar Far Cry 2 heeft qua spelelementen en verhaal aanzienlijk meer diepgang dan dit fantastische wat mijn opa zou noemen gooi en smijtspel. De aanrader van vandaag? Geen van deze kaarten. Echt, doe maar niet. Al zijn ze voor een fractie van een tientje te krijgen, het is je geld niet waard. Tenzij je alleen maar op zoek bent naar een HDMI-poort(dan praat ik sowieso niet met je want HDMI poorten zijn voor saaie mensen) en je legt niet meer dan twee euro neer, misschien. De uitzondering hier is de GT 140, die mij verbaasde met de prestaties. Ondanks dat deze kaart extra niet aan te raden is vanwege de 512mb VRAM en de 6-pin stekker die nodig is is deze misschien wat meer waard. Al is het alleen maar vanwege die mooie rode printplaat.